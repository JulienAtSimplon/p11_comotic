""" Comotic project by Julien Stacchetti
module created at 2020-05-19
Doit disparaitre apres refactoring de DBQueryMySql avec des décorateurs
"""

query_kpi1 = (f"""  SELECT COUNT(id)
                    FROM solicitations 
                    WHERE   account_id = %s
                            AND created_at BETWEEN %s AND %s
                            AND TIME(created_at) BETWEEN %s AND %s
                            AND DAYOFWEEK(created_at) BETWEEN %s AND %s
                    """)

query_kpi2 = (f"""  SELECT  s.interlocutor_id,
                            s.id AS solicit_id,
                            s.created_at AS solicitation_date,
                            r.created_at AS replie_date,
                            TIMEDIFF(r.created_at, s.created_at) as response_time
                    FROM solicitations AS s
                    JOIN replies AS r ON s.id = r.solicitation_id
                    WHERE       r.created_at = (
                                SELECT MAX(created_at)
                                FROM replies WHERE solicitation_id = s.id
                                GROUP BY solicitation_id
                                )
                            AND s.account_id = %s
                            AND s.created_at BETWEEN %s AND %s
                            AND TIME(s.created_at) BETWEEN %s AND %s
                            AND DAYOFWEEK(s.created_at) BETWEEN %s AND %s
                    GROUP BY solicit_id, replie_date
                    ORDER BY response_time
                    """)

query_kpi3 = query_kpi2 # + traitement post-requête

query_kpi4 = ("""WIP""") # Comment déterminer qu'une réponse est automatique ?

query_kpi5 = (f"""  SELECT COUNT(DISTINCT interlocutor_id)
                    FROM solicitations 
                    WHERE   account_id = %s
                            AND created_at BETWEEN %s AND %s
                    """)

query_kpi6 = (f"""  SELECT interlocutor_id, COUNT(interlocutor_id)
                    FROM solicitations 
                    WHERE   account_id = %s
                            AND created_at BETWEEN %s AND %s
                    GROUP BY interlocutor_id 
                    ORDER BY COUNT(interlocutor_id) DESC, interlocutor_id ASC
                    LIMIT 10
                    """)

query_kpi7 = (f"""  SELECT interlocutor_id, COUNT(score)
                    FROM solicitations 
                    WHERE   score = 1
                            AND account_id = %s
                            AND created_at BETWEEN %s AND %s
                    GROUP BY interlocutor_id 
                    ORDER BY COUNT(score) DESC, interlocutor_id ASC
                    LIMIT 10
                    """)

query_kpi8 = (f"""  SELECT interlocutor_id, COUNT(score)
                    FROM solicitations 
                    WHERE   score = 4
                            AND account_id = %s
                            AND created_at BETWEEN %s AND %s
                    GROUP BY interlocutor_id 
                    ORDER BY COUNT(score) DESC, interlocutor_id ASC
                    LIMIT 10
                    """)

query_kpi9 = (f"""  SELECT score, COUNT(score)
                    FROM solicitations 
                    WHERE   account_id = %s
                            AND created_at BETWEEN %s AND %s
                            AND TIME(created_at) BETWEEN %s AND %s
                            AND DAYOFWEEK(created_at) BETWEEN %s AND %s
                    GROUP BY score
                    ORDER BY COUNT(score) DESC
                    """)

query_alt_kpi1 = (f"""  SELECT interlocutor_id, AVG(score)
                        FROM solicitations 
                        WHERE   account_id = %s
                                    AND created_at BETWEEN %s AND %s
                                AND TIME(created_at) BETWEEN %s AND %s
                                AND DAYOFWEEK(created_at) BETWEEN %s AND %s
                        GROUP BY interlocutor_id
                        ORDER BY AVG(score) DESC, interlocutor_id ASC
                        LIMIT 10
                        """)
""" Comotic project by Julien Stacchetti
module created at 2020-05-13
last update at 2020-05-20 """

import datetime
import DBProtocolMySql as db

account_id = 19 # KPI d'exemple pour l'utilisateur connecté avec l'ID 19
date_start = '2020-01-01 00:00:00'
date_end = '2021-01-01 00:00:00'
time_start = '00:00:00'
time_end = '23:59:59'
day_start = 1
day_end = 7

param_tuple = (account_id, date_start, date_end, time_start, time_end, day_start, day_end)
param_tuple_short = (account_id, date_start, date_end)

### Requêtes des 9 KPI
query_kpi1 = (f"""  SELECT COUNT(id)
                    FROM solicitations 
                    WHERE   account_id = %s
                            AND parent_id IS NULL
                            AND created_at BETWEEN %s AND %s
                            AND TIME(created_at) BETWEEN %s AND %s
                            AND DAYOFWEEK(created_at) BETWEEN %s AND %s
                    """)

query_kpi2 = (f"""  SELECT  s.interlocutor_id,
                            s.id AS solicit_id,
                            s.created_at AS solicitation_date,
                            r.created_at AS replie_date,
                            TIMEDIFF(r.created_at, s.created_at) as response_time
                    FROM solicitations AS s
                    JOIN replies AS r ON s.id = r.solicitation_id
                    WHERE       r.created_at = (
                                SELECT MIN(created_at)
                                FROM replies WHERE solicitation_id = s.id
                                GROUP BY solicitation_id
                                )
                            AND parent_id IS NULL
                            AND s.account_id = %s
                            AND s.created_at BETWEEN %s AND %s
                            AND TIME(s.created_at) BETWEEN %s AND %s
                            AND DAYOFWEEK(s.created_at) BETWEEN %s AND %s
                    GROUP BY solicit_id, replie_date
                    ORDER BY response_time
                    """)

query_kpi3 = query_kpi2 # + traitement post-requête

query_kpi4 = ("""WIP""") # Comment déterminer qu'une réponse est automatique ?

query_kpi5 = (f"""  SELECT COUNT(DISTINCT interlocutor_id)
                    FROM solicitations 
                    WHERE   account_id = %s
                            AND parent_id IS NULL
                            AND created_at BETWEEN %s AND %s
                    """)

query_kpi6 = (f"""  SELECT interlocutor_id, COUNT(interlocutor_id)
                    FROM solicitations 
                    WHERE   account_id = %s
                            AND parent_id IS NULL
                            AND created_at BETWEEN %s AND %s
                    GROUP BY interlocutor_id 
                    ORDER BY COUNT(interlocutor_id) DESC, interlocutor_id ASC
                    LIMIT 10
                    """)

query_kpi7 = (f"""  SELECT interlocutor_id, COUNT(score)
                    FROM solicitations 
                    WHERE   score = 1
                            AND parent_id IS NULL
                            AND account_id = %s
                            AND created_at BETWEEN %s AND %s
                    GROUP BY interlocutor_id 
                    ORDER BY COUNT(score) DESC, interlocutor_id ASC
                    LIMIT 10
                    """)

query_kpi8 = (f"""  SELECT interlocutor_id, COUNT(score)
                    FROM solicitations 
                    WHERE   score = 4
                            AND parent_id IS NULL
                            AND account_id = %s
                            AND created_at BETWEEN %s AND %s
                    GROUP BY interlocutor_id 
                    ORDER BY COUNT(score) DESC, interlocutor_id ASC
                    LIMIT 10
                    """)

query_kpi9 = (f"""  SELECT score, COUNT(score)
                    FROM solicitations 
                    WHERE   account_id = %s
                            AND parent_id IS NULL
                            AND created_at BETWEEN %s AND %s
                            AND TIME(created_at) BETWEEN %s AND %s
                            AND DAYOFWEEK(created_at) BETWEEN %s AND %s
                    GROUP BY score
                    ORDER BY COUNT(score) DESC
                    """)


### KPI n'écésitant une requete alternative pour les "grouper par"
query_kpi3_g3 = ("""""")

concur = db.fast_con_cur(db.db_config)

q_result_kpi1 = db.fast_fetchall(concur, query_kpi1, param_tuple)
q_result_kpi2 = db.fast_fetchall(concur, query_kpi2, param_tuple)
q_result_kpi3 = db.fast_fetchall(concur, query_kpi3, param_tuple)
# q_result_kpi4 = db.fast_fetchall(concur, query_kpi4, param_tuple_short)
q_result_kpi5 = db.fast_fetchall(concur, query_kpi5, param_tuple_short)
q_result_kpi6 = db.fast_fetchall(concur, query_kpi6, param_tuple_short)
q_result_kpi7 = db.fast_fetchall(concur, query_kpi7, param_tuple_short)
q_result_kpi8 = db.fast_fetchall(concur, query_kpi8, param_tuple_short)
q_result_kpi9 = db.fast_fetchall(concur, query_kpi9, param_tuple)


### KPI nécessitant un traitement post-requête
def kpi3_postproc(q_result):
    fast_time = datetime.timedelta(0)
    for elt in q_result:
        fast_time += elt[4]

    result = fast_time / len(q_result)
    return result


### KPI supplémentaires
query_alt_kpi1 = (f"""  SELECT interlocutor_id, AVG(score)
                        FROM solicitations 
                        WHERE   account_id = %s
                                AND parent_id IS NULL
                                AND created_at BETWEEN %s AND %s
                                AND TIME(created_at) BETWEEN %s AND %s
                                AND DAYOFWEEK(created_at) BETWEEN %s AND %s
                        GROUP BY interlocutor_id
                        ORDER BY AVG(score) DESC, interlocutor_id ASC
                        LIMIT 10
                        """) # Moyennes de prioritées par interlocuteurs

q_result_alt_kpi1 = db.fast_fetchall(concur, query_alt_kpi1, param_tuple)


### Affichage dans le terminal
def verification_terminal(): 
    print("\n--- KPI Comotic - Base de donées initiale (dump) - pour l'utilisateur connecté avec l'ID 19 ---")
    print("KPI 1 - \"Nombre de message reçus\" : ", q_result_kpi1[0][0])
    print("KPI 2 - \"Temps de réponse\" : ", q_result_kpi2)
    print("KPI 3 - \"Temps de réponse moyen\" : ", kpi3_postproc(q_result_kpi3))
    print("KPI 4 - \"Taux de réponse automatique\" : ", "WIP : Comment déterminer qu'une réponse est automatique ?")
    print("KPI 5 - \"Interlocauteurs\" : ", q_result_kpi5[0][0])
    print("KPI 6 - \"Interlocauteurs les plus actifs\" : ", q_result_kpi6)
    print("KPI 7 - \"Interlocauteurs les moins qualitatifs\" : ", q_result_kpi7)
    print("KPI 8 - \"Interlocauteurs les plus critiques\" : ", q_result_kpi8)
    print("KPI 9 - \"Répartition des priorités\" : ", q_result_kpi9)
    print("\n--- KPI bonus ---")
    print("KPI bonus 1 - \"Moyennes de prioritées par interlocuteurs\" : ", q_result_alt_kpi1)


if __name__ == "__main__":
#     verification_terminal()
    pass

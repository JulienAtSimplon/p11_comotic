""" Comotic project by Julien Stacchetti
module created at 2020-05-12
last update at 2020-05-19 """

import os, time

def pipenv_start():
    os.system("pipenv shell")


def docker_up():
    os.system("sudo docker-compose up -d")


def docker_down():
    os.system("sudo docker-compose down")


def flask_run():
    os.system("flask run")


def start_day():
#    pipenv_start()
    time.sleep(4)
    docker_up()
    time.sleep(4)
    flask_run()


def end_day():
    docker_down()

if __name__ == "__main__":
    start_day()
    #end_day()
""" Comotic project by Julien Stacchetti
module created at 2020-05-18
last update at 2020-05-19 """

from flask import Flask, render_template, redirect, request, url_for
import DBProtocolMySql as db
import DBQueryMySql as q
import DBAltQuery as a

app = Flask(__name__)

concur = db.fast_con_cur(db.db_config)

@app.route("/", methods=["GET"])
def get_home():
    account_id = request.args.get("account_id", 19)
    date_start = request.args.get("date_start", '2020-01-01 00:00:00')
    date_end = request.args.get("date_end", '2021-01-01 00:00:00')
    time_start = request.args.get("time_start", '00:00:00')
    time_end = request.args.get("time_end", '23:59:59')
    day_start = request.args.get("day_start", 1)
    day_end = request.args.get("day_end", 7)
    
    param_tuple = (account_id, date_start, date_end, time_start, time_end, day_start, day_end)
    param_tuple_short = (account_id, date_start, date_end)
    
    if request.args.get("parent_id", 'True') == 'True': # a refactor avec des décorateurs
        parent_id = request.args.get("parent_id", 'True')
        return render_template(
            "index.html",
            titre = """Interface prototypale d'affichage des statistiques "team" de Comotic""",
            parent_id = parent_id,
            account_id = account_id,
            date_start = date_start,
            date_end = date_end,
            time_start = time_start,
            time_end = time_end,
            day_start = day_start,
            day_end = day_end,
            KPI1 = db.fast_fetchall(concur, q.query_kpi1, param_tuple)[0][0],
            KPI2 = db.fast_fetchall(concur, q.query_kpi2, param_tuple),
            KPI3 = q.kpi3_postproc(db.fast_fetchall(concur, q.query_kpi2, param_tuple)),
            KPI4 = "WIP", # db.fast_fetchall(concur, q.query_kpi4, param_tuple)[0][0],
            KPI5 = db.fast_fetchall(concur, q.query_kpi5, param_tuple_short)[0][0],
            KPI6 = db.fast_fetchall(concur, q.query_kpi6, param_tuple_short),
            KPI7 = db.fast_fetchall(concur, q.query_kpi7, param_tuple_short),
            KPI8 = db.fast_fetchall(concur, q.query_kpi8, param_tuple_short),
            KPI9 = db.fast_fetchall(concur, q.query_kpi9, param_tuple),
            KPIA1 = db.fast_fetchall(concur, q.query_alt_kpi1, param_tuple),
            sql_query1 = q.query_kpi1,
            sql_query2 = q.query_kpi2,
            sql_query3 = q.query_kpi3,
            sql_query4 = q.query_kpi4,
            sql_query5 = q.query_kpi5,
            sql_query6 = q.query_kpi6,
            sql_query7 = q.query_kpi7,
            sql_query8 = q.query_kpi8,
            sql_query9 = q.query_kpi9,
            sql_query_alt1 = q.query_alt_kpi1,
            )

    else:
        parent_id = request.args.get("parent_id", 'False')
        return render_template(
            "index.html",
            titre = """Interface prototypale d'affichage des statistiques "team" de Comotic""",
            parent_id = parent_id,
            account_id = account_id,
            date_start = date_start,
            date_end = date_end,
            time_start = time_start,
            time_end = time_end,
            day_start = day_start,
            day_end = day_end,
            KPI1 = db.fast_fetchall(concur, a.query_kpi1, param_tuple)[0][0],
            KPI2 = db.fast_fetchall(concur, a.query_kpi2, param_tuple),
            KPI3 = q.kpi3_postproc(db.fast_fetchall(concur, a.query_kpi2, param_tuple)),
            KPI4 = "WIP", # db.fast_fetchall(concur, a.query_kpi4, param_tuple)[0][0],
            KPI5 = db.fast_fetchall(concur, a.query_kpi5, param_tuple_short)[0][0],
            KPI6 = db.fast_fetchall(concur, a.query_kpi6, param_tuple_short),
            KPI7 = db.fast_fetchall(concur, a.query_kpi7, param_tuple_short),
            KPI8 = db.fast_fetchall(concur, a.query_kpi8, param_tuple_short),
            KPI9 = db.fast_fetchall(concur, a.query_kpi9, param_tuple),
            KPIA1 = db.fast_fetchall(concur, a.query_alt_kpi1, param_tuple),
            sql_query1 = a.query_kpi1,
            sql_query2 = a.query_kpi2,
            sql_query3 = a.query_kpi3,
            sql_query4 = a.query_kpi4,
            sql_query5 = a.query_kpi5,
            sql_query6 = a.query_kpi6,
            sql_query7 = a.query_kpi7,
            sql_query8 = a.query_kpi8,
            sql_query9 = a.query_kpi9,
            sql_query_alt1 = a.query_alt_kpi1,
            )
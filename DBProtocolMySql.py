""" Comotic project by Julien Stacchetti
module created at 2020-05-12
last update at 2020-05-19 : code unmodified """

import os, mysql.connector
from mysql.connector import errorcode
from dotenv import load_dotenv

load_dotenv()

environment_name = os.getenv("DB_ENVIRO", None)
print("Environment :", environment_name)

db_config = {
    'user': os.getenv("DB_USER", None),
    'password': os.getenv("DB_PASSWORD", None),
    'host': os.getenv("DB_HOST", None),
    'port': os.getenv("DB_PORT", None),
    'database': os.getenv("DB_DBNAME", None),
    'raise_on_warnings': True
    }


def connection(config_dict):
    """ Connection method... """
    try:
        cnx = mysql.connector.connect(**config_dict)
        print("Connection established with this object :", cnx)
    
    except mysql.connector.Error as err:
        if err.errno == errorcode.ER_ACCESS_DENIED_ERROR:
            print("Something is wrong with your user name or password")
        elif err.errno == errorcode.ER_BAD_DB_ERROR:
            print("Database does not exist")
        else:
            print(err)
    
    return cnx


def disconnection(connection):
    """ Disconnection method... """
    try:
        connection.close()
        print("Disconnected")
    except:
        print("Failed to disconnect")


def fast_con_cur(config_dict): # Return connection & cursor as tuple
    cnx = connection(config_dict)
    cursor = cnx.cursor()
    return cnx, cursor


def fast_close(con_cur_tuple):
    con_cur_tuple[1].close()
    con_cur_tuple[0].close()


def fast_fetchall(con_cur_tuple, sql_query, tuple_var = None):
    con_cur_tuple[1].execute(sql_query, tuple_var)
    try:
        result = con_cur_tuple[1].fetchall()
    except:
        result = "WIP"
    
    return result
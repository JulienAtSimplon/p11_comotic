""" Comotic project by Julien Stacchetti
unit tests created at 2020-05-13
last update at 2020-05-13 """

import pytest
import DBProtocolMySql as db


def test_db_connection():
    assert db.connection(db.db_config) == True


def test_db_discon():
    cnx = db.connection(db.db_config) 
    assert db.disconnection(cnx) == True